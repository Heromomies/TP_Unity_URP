﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorBlendTree : MonoBehaviour
{
    public Animator anim;

    private float _velocity = 0.0f;
    public float acceleration = 0.1f;
    public float deceleration = 0.5f;
    private int _velocityHash;

    private void Start()
    {
        _velocityHash = Animator.StringToHash("Velocity");
    }

    private void Update()
    {
        bool forwardPressed = Input.GetKey(KeyCode.Z);

        if (forwardPressed && _velocity < 1f)
        {
            _velocity += Time.deltaTime * acceleration;
        }
        else
        {
            if (_velocity > 0)
            {
                _velocity -= Time.deltaTime * deceleration;
            }
        }
        
        anim.SetFloat(_velocityHash, _velocity);
    }
}
