﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorBlendTree2D : MonoBehaviour
{
    [SerializeField] private Animator anim;

    private float _velocityX = 0.0f;
    private float _velocityY = 0.0f;
    public float acceleration = 0.1f;
    public float deceleration = 0.5f;
    private int _velocityHashX;
    private int _velocityHashY;

    private void Start()
    {
        _velocityHashX = Animator.StringToHash("VelocityX");
        _velocityHashY = Animator.StringToHash("VelocityY");
    }

    public void Update()
    {
        bool forwardPressed = Input.GetKey(KeyCode.Z);
        bool leftPressed = Input.GetKey(KeyCode.Q);
        bool rightPressed = Input.GetKey(KeyCode.D);

        if (forwardPressed && _velocityY < 1)
        {
            _velocityY += Time.deltaTime * acceleration;
        }
        if (leftPressed && _velocityX > -1)
        {
            _velocityX -= Time.deltaTime * acceleration;
            transform.Rotate(Vector3.up * _velocityX, Space.World);
        }
        if (rightPressed && _velocityX < 1)
        {
            _velocityX += Time.deltaTime * acceleration;
            transform.Rotate(Vector3.up * _velocityX, Space.World);
        }
        if (!forwardPressed && _velocityY > 0)
        {
            _velocityY -= Time.deltaTime * deceleration;
        }
        if (!leftPressed && _velocityX < 0)
        {
            _velocityX += Time.deltaTime * deceleration;
        }
        if (!rightPressed && _velocityX > 0)
        {
            _velocityX -= Time.deltaTime * deceleration;
        }
        anim.SetFloat(_velocityHashX, _velocityX);
        anim.SetFloat(_velocityHashY, _velocityY);
    }
}
